## Empty set up by command

```
    sh ./proto-install.sh
```

## Execute generate Model by

```
    sh ./generate.sh
```
### Package name is `mpb`

install

[https://github.com/grpc-ecosystem/grpc-gateway/issues/422](
https://github.com/grpc-ecosystem/grpc-gateway/issues/422)

Make sure you grab the latest version
```bash
curl -OL https://github.com/google/protobuf/releases/download/v3.2.0/protoc-3.2.0-linux-x86_64.zip
```

Unzip
```sh
unzip protoc-3.2.0-linux-x86_64.zip -d protoc3
```

Move protoc to /usr/local/bin/
```sh
sudo mv protoc3/bin/* /usr/local/bin/
```

Move protoc3/include to /usr/local/include/
```sh
sudo mv protoc3/include/* /usr/local/include/
```

Optional: change owner
sudo chown [user] /usr/local/bin/protoc
sudo chown -R [user] /usr/local/include/google


## demo trace
`"{"ip":"69.171.224.1","country_short":"US","country_long":"United States","region":"California","city":"Menlo Park","latitude":37.459,"longitude":-122.1781}"`