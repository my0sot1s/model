#!/bin/bash
# #build for common
protoc common/common.proto -I. --go_out=../../../

protoc -I/usr/local/include -I. -I$GOPATH/src \
	account/account.proto service/service.proto seat/seat.proto \
	-I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--swagger_out=logtostderr=true,allow_merge=true:.

# protoc account/account.proto -I. --go_out=plugins=grpc:.
#  --go_out=plugins=grpc:.
protoc -I/usr/local/include -I. -I$GOPATH/src \
  -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
	--go_out=plugins=grpc:. account/account.proto

# protoc user/user.proto -I. --go_out=plugins=grpc:.

# protoc store/store.proto -I. --go_out=plugins=grpc:.

# protoc seat/seat.proto -I. \
# 	-I$GOPATH/src \
#   -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
# 	--go_out=plugins=grpc:.

# protoc service/service.proto -I. \
# 	-I$GOPATH/src \
#   -I$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
# 	--go_out=plugins=grpc:.

./convert account/account.pb.go service/service.pb.go seat/seat.pb.go
# copy file
echo copy file
cp apidocs.swagger.json ../devconfigs
echo all done!